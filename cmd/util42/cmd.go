package main

import (
	"gitlab.com/Ma_124/util42"
	"gopkg.in/alecthomas/kingpin.v2"
	"os"
)

func main() {
	app := kingpin.New("util42", "42")
	app.Command("interactive", "Launches an interactive shell.")
	for _, u := range util42.Utilities {
		cmd := app.Command(u.Name, u.String() + "\n\n" + u.Doc)
		for _, p := range u.Params {
			arg := cmd.Arg(p.K, "")
			switch p.T {
			case "str":
				arg.String()
			default:
				arg.String()
			}
		}
	}

	cmd := kingpin.MustParse(app.Parse(os.Args[1:]))
	if cmd == "interactive" {
		err := Interactive()
		if err != nil {
			kingpin.Fatalf("%s", err)
		}
	}
	for _, c := range app.Model().Commands {
		if c.Name != cmd {
			args := make([]interface{}, len(c.Args))
			for i, a := range c.Args {
				v := a.Value
				if _, ok := v.(kingpin.Getter); !ok {
					kingpin.Fatalf("Internal error: %q must be gettable (implement `Get() interface{}`)", a.Name)
				}
				args[i] = v.(kingpin.Getter).Get()
			}
			break
		}
	}
}

func Interactive() error {
	// TODO
	panic("not yet implemented")
}
