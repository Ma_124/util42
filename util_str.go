package util42

import (
	"strings"
	"unicode"
)

// Lower returns a copy of the string inp with all Unicode letters mapped to their lower case.
func Lower(inp string) (out string) {
	return strings.ToLower(inp)
}

// Upper returns a copy of the string inp with all Unicode letters mapped to their upper case.
func Upper(inp string) (out string) {
	return strings.ToUpper(inp)
}

// Fields splits the string s around each instance of one or more consecutive white space
// characters, as defined by unicode.IsSpace, returning a slice of substrings of s or an
// empty slice if s contains only white space.
func Fields(inp string) (out []string) {
	return strings.Fields(inp)
}

// Capitalize returns a copy of the string inp with the first Unicode letter mapped to its upper case.
//
// NOTE: does not handle two byte letters correctly
func Capitalize(inp string) (out string) {
	if unicode.IsLower(rune(inp[0])) {
		return string(unicode.ToUpper(rune(inp[0]))) + inp[1:]
	}
	return inp
}
