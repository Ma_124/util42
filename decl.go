package util42

import "bytes"

type UtilityFn func(a ...interface{}) (interface{}, error)

type FieldDecl struct {
	K string
	T string
}

func (f FieldDecl) String() string {
	return f.K + ": " + f.T
}

type UtilityDecl struct {
	Name string
	Doc  string

	Params  []FieldDecl
	Returns []FieldDecl

	Fn UtilityFn
}

func (f UtilityDecl) String() string {
	var buf bytes.Buffer
	buf.WriteString(f.Name)
	if len(f.Params) != 0 {
		for _, p := range f.Params {
			buf.WriteString(" <")
			buf.WriteString(p.K)
			buf.WriteString(": ")
			buf.WriteString(typAlias(p.T))
			buf.WriteByte('>')
		}
	}
	if len(f.Returns) != 0 {
		buf.WriteString(" →")
		for _, p := range f.Returns {
			buf.WriteString(" <")
			buf.WriteString(p.K)
			buf.WriteString(": ")
			buf.WriteString(typAlias(p.T))
			buf.WriteByte('>')
		}
	}
	return buf.String()
}

func (f UtilityDecl) GoString() string {
	var buf bytes.Buffer
	buf.WriteString(f.Name)
	buf.WriteByte('(')
	if len(f.Params) != 0 {
		for i, p := range f.Params {
			if i != 0 {
				buf.WriteString(", ")
			}
			buf.WriteString(p.K)
			buf.WriteByte(' ')
			buf.WriteString(typAlias(p.T))
		}
	}
	buf.WriteByte(')')
	if len(f.Returns) != 0 {
		buf.WriteString(" (")
		for i, p := range f.Returns {
			if i != 0 {
				buf.WriteString(", ")
			}
			buf.WriteString(p.K)
			buf.WriteByte(' ')
			buf.WriteString(typAlias(p.T))
		}
		buf.WriteByte(')')
	}
	return buf.String()
}

func typAlias(t string) string {
	switch t {
	case "string":
		return "str"
	default:
		return t
	}
}
