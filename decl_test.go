package util42

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestFuncDecl_String(t *testing.T) {
	tests := []struct {
		name string
		decl UtilityDecl

		wantStr   string
		wantGostr string
	}{
		{
			"noop",
			UtilityDecl{
				Name:    "noop",
				Doc:     "Does nothing",
				Params:  nil,
				Returns: []FieldDecl{},
			},
			"noop",
			"noop()",
		},
		{
			"write",
			UtilityDecl{
				Name:    "write",
				Doc:     "Writes a string to stdout",
				Params:  []FieldDecl{{"v", "string"}},
				Returns: nil,
			},
			"write <v: str>",
			"write(v str)",
		},
		{
			"read",
			UtilityDecl{
				Name:    "read",
				Doc:     "Read a string from stdin",
				Params:  nil,
				Returns: []FieldDecl{{"v", "string"}},
			},
			"read → <v: str>",
			"read() (v str)",
		},
		{
			"prompt",
			UtilityDecl{
				Name:    "prompt",
				Doc:     "Read a string from stdin after writing <prompt> to stdout",
				Params:  []FieldDecl{{"prompt", "string"}},
				Returns: []FieldDecl{{"v", "string"}},
			},
			"prompt <prompt: str> → <v: str>",
			"prompt(prompt str) (v str)",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := UtilityDecl{
				Name:    tt.decl.Name,
				Doc:     tt.decl.Doc,
				Params:  tt.decl.Params,
				Returns: tt.decl.Returns,
			}
			assert.Equal(t, tt.wantStr, f.String())
			assert.Equal(t, tt.wantGostr, f.GoString())
		})
	}
}
